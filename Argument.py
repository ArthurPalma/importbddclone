import csv
import argparse
import os
import sys

# define the arguments that must entered to run the program
def Args():
    parser = argparse.ArgumentParser(
    description='Traduis le contenu du fichier CSV dans un autre fichier CSV')
    # Permet d'entrer le fichier à traduire et le fichier recevant la traduction en paramètres
    parser.add_argument('infile', help='chemin du fichier entré')
    parser.add_argument('--delimiter', help='délimiteur du fichier original (Par défaut "|" )')
    parser.add_argument('BDD', help='chemin de la base de données')
    return parser.parse_args()


