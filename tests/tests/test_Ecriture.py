import unittest
from Ecriture import copieFichier
from Lecture import lectureFichierChaine

class TestEcritureFichierFonction(unittest.TestCase):

    maxDiff =
    def test_copieNouveauFichier(self):
        dictionnaire = [{'address': '3822 Omar Square Suite 257 Port Emily, OK 43251',
                         'name': 'Smith', 'firstname': 'Jerome', 'immat':'OVC-568', 
                         'date_immat': '2012-05-03', 'vin': '9780082351764','marque':'Williams Inc',
                         'denomination':'Enhanced well-modulated moderator',
                         'couleur':'LightGoldenRodYellow','carrosserie':'45-1743376',
                         'categorie':'34-7904216','cylindree':'3462','energy':'37578077',
                         'places':'32','poids':'3827','puissance':'110',
                         'type_variante_version':'Inc, 92-3625175, 79266482'}]

        copieFichier("test.csv",dictionnaire)
        
        resultat =     [{'adresse_titulaire': '3822 Omar Square Suite 257 Port Emily, OK 43251',
                         'nom': 'Smith', 'prenom': 'Jerome', 'immatriculation':'OVC-568', 
                         'date_immatriculation': '03-05-2012', 'vin': '9780082351764','marque':'Williams Inc',
                         'denomination_commerciale':'Enhanced well-modulated moderator',
                         'couleur':'LightGoldenRodYellow','carrosserie':'45-1743376',
                         'categorie':'34-7904216','cylindree':'3462','energie':'37578077',
                         'places':'32','poids':'3827','puissance':'110',
                         'type':'Inc','variante': '92-3625175','verison': '79266482'}]
        self.assertEqual(lectureFichierChaine("test.csv",';'),resultat)