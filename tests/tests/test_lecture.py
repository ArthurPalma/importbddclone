import unittest
from Lecture import lectureFichierChaine

class TestLectureFichierFonction(unittest.TestCase):
    def test_Lecture(self):
        resultat = [{"Prenom":"Lucas","Groupe":'3',"Annee":"2eme"},
                    {"Prenom":"Arthur","Groupe":'3',"Annee":"2eme"}]
        self.assertEqual(lectureFichierChaine("fichier.csv",'|'), resultat)
