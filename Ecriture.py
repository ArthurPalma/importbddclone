import csv
import datetime
import logging




def copieFichier(cheminNouveauFichier, dictionnaire):
    with open(cheminNouveauFichier, 'w') as outfile:
        logging.info('Modification du fichier CSV')
        fieldnames = ['adresse_titulaire', 'nom', 'prenom', 'immatriculation', 'date_immatriculation', 'vin', 'marque', 'denomination_commerciale',
                      'couleur', 'carrosserie', 'categorie', 'cylindree', 'energie', 'places', 'poids', 'puissance', 'type', 'variante', 'version']
        writer = csv.DictWriter(outfile, fieldnames=fieldnames, delimiter=';')
        writer.writeheader()
        for row in dictionnaire:
            liste_type_variante_version = row["type_variante_version"].split(
                ', ')
            date = datetime.datetime.strptime(row['date_immat'], '%Y-%m-%d')
            row['date_immat'] = date.strftime('%d-%m-%Y')

            writer.writerow({'adresse_titulaire': row['address'], 'nom': row['name'],
                             'prenom': row['firstname'], 'immatriculation': row['immat'],
                             'date_immatriculation': row['date_immat'], 'vin': row['vin'],
                             'marque': row['marque'], 'denomination_commerciale': row['denomination'],
                             'couleur': row['couleur'], 'carrosserie': row['carrosserie'],
                             'categorie': row['categorie'], 'cylindree': row['cylindree'],
                             'energie': row['energy'], 'places': row['places'], 'poids': row['poids'],
                             'puissance': row['puissance'], 'type': liste_type_variante_version[0],
                             'variante': liste_type_variante_version[1], 'version': liste_type_variante_version[2]
                             })
