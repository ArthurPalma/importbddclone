import argparse
import os
import sys
from Lecture import lectureFichierChaine
from Ecriture import copieFichier
import sqlite3
import csv
import logging


def Insert(BDD, instructions, cheminfichier):
    logging.info('Création de la base de donnée')
    conn = sqlite3.connect(BDD)
    cursor = conn.cursor()
    logging.info('Remplissage des tables SQL')
    with open(instructions) as instruct:
        cursor.execute(instruct.read())
    liste = lectureFichierChaine(cheminfichier, ";")
    for ligne in liste:
        cursor.execute('INSERT INTO siv VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ligne)
    conn.commit()
    conn.close()

def UpdateLine(bddname,my_tuple,immatriculation):
    connection = sqlite3.Connection(bddname)
    cursor = connection.cursor()
    cursor.execute("Delete from voiture where immatriculation = ? ",(immatriculation,))
    connection.commit()
    insertItInBdd(bddname,my_tuple)
    connection.close()


if __name__ == "__main__":
    logging.basicConfig(filename='myapp.log', format='%(asctime)s - %(levelname)s:%(message)s', level=logging.DEBUG)
    parser = argparse.ArgumentParser(
        description='Traduis le contenu du fichier CSV dans un autre fichier CSV')
    # Permet d'entrer le fichier à traduire et le fichier recevant la traduction en paramètres
    parser.add_argument('infile', help='chemin du fichier entré')
    parser.add_argument('outfile', help='chemin du fichier sorti')
    parser.add_argument('--delimiter', help='délimiteur du fichier original (Par défaut "|" )')
    parser.add_argument('BDD', help='chemin de la base de données')
    args = parser.parse_args()
    # Vérifie que le chemin mène à un fichier valide
    if not os.path.isfile(args.infile):
        print("ERROR 101: %s n'est pas un fichier valide !" % args.infile)
        sys.exit(101)
    if args.delimiter:
        liste = lectureFichierChaine(args.infile,args.delimiter)
    else:
        liste = lectureFichierChaine(args.infile,'|')
    Insert(args.BDD, "instructions.sql",args.outfile)
