import csv
import logging

# Function used to read the CSV file entered in argument
def lectureFichierChaine(cheminFichier,delimiteur):
    """
    Read a csv file from given filepath

    Parameters
    ----------
    cheminfichier : str
        The filepath of the csv file to open
    delimiteur : str
        The delimiter used to read the csv file

    Returns
    -------
    list
        a list of all csv rows as list
    """

    logging.info('Lecture du fichier CSV')
    liste = []
    i=0
    with open(cheminFichier) as cvsfile:
        lecteur = csv.reader(cvsfile, delimiter=delimiteur)
        for ligne in lecteur:
            if i>0:
               liste.append(ligne)
            i=i+1
        return liste
